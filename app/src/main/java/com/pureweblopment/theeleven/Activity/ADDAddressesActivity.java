package com.pureweblopment.theeleven.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.theeleven.Global.Global;
import com.pureweblopment.theeleven.Global.SendMail;
import com.pureweblopment.theeleven.Global.SharedPreference;
import com.pureweblopment.theeleven.Global.StaticUtility;
import com.pureweblopment.theeleven.Global.Typefaces;
import com.pureweblopment.theeleven.Model.Country;
import com.pureweblopment.theeleven.Model.State;
import com.pureweblopment.theeleven.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class ADDAddressesActivity extends AppCompatActivity implements
        View.OnClickListener {

    Context context = ADDAddressesActivity.this;

    String strCountry = "", strState = "";
    private Country country;
    private State state;
    ArrayList<Country> countries = new ArrayList<>();
    ArrayList<State> states = new ArrayList<>();

    Button btnAddAddress;

    EditText editFirstName, editLastName, editAddress, editLandmark, editPincode, editCity, editPhoneNo;
    TextView txtAddShippingAddress, txtAddBillingAddress;

    LinearLayout llAddBillingAddress;
    CheckBox checkBoxBilling, checkBoxisShipping;

    RelativeLayout relativeProgress;

    CoordinatorLayout coodinator;
    Boolean ischeck = false;

    ImageView imageCartBack;
    LinearLayout llToolbar;
    String ActivityType = "", AddressType = "", Type = "";

    AutoCompleteTextView actCountry, actState;
    TextView txtAddressName;

    FrameLayout flCountry, flState;
    TextView txtIsShipping;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.manageBackPress(false);
        MainActivity.isCheckoutBack = "1";
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_add_addresses);

        eventBus.register(this);

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor
                    (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                            StaticUtility.ThemePrimaryColor))));
        }

        ActivityType = getIntent().getStringExtra("ActivityType");
        Type = getIntent().getStringExtra("Type");
        Initialization();
        TypeFace();
        OnClickListener();
        AppSetting();
        setDynamicString();

        chanageEditTextBorder(editFirstName);
        chanageEditTextBorder(editLastName);
        chanageEditTextBorder(editAddress);
        chanageEditTextBorder(editLandmark);
        chanageEditTextBorder(editPincode);
        chanageEditTextBorder(editCity);
        chanageEditTextBorder(editPhoneNo);
        chanageButton(btnAddAddress);
        chanageFramelayoutBorder(flCountry, actCountry);
        chanageFramelayoutBorder(flState, actState);

        getCountryAPI();
        if (actCountry.length() > 0) {
            actState.setEnabled(true);
            getStateAPI();
        } else {
            actState.setEnabled(false);
        }

        actCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    actState.setEnabled(true);
                    getStateAPI();
                } else {
                    actState.setEnabled(false);
                    actState.setText("");
                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                            StaticUtility.sSELECT_STATE) != null) {
                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                StaticUtility.sSELECT_STATE).equals("")) {
                            actState.setHint(SharedPreference.GetPreference(context,
                                    StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE));
                        } else {
                            actState.setHint(getString(R.string.select_state));
                        }
                    } else {
                        actState.setHint(getString(R.string.select_state));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    //region setDynamicString
    private void setDynamicString() {
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sFIREST_NAME) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sFIREST_NAME).equals("")) {
                editFirstName.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sFIREST_NAME));
            } else {
                editFirstName.setHint(getString(R.string.first_name));
            }
        } else {
            editFirstName.setHint(getString(R.string.first_name));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sLAST_NAME).equals("")) {
                editLastName.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sLAST_NAME));
            } else {
                editLastName.setHint(getString(R.string.last_name));
            }
        } else {
            editLastName.setHint(getString(R.string.last_name));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sADDRESS).equals("")) {
                editAddress.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sADDRESS));
            } else {
                editAddress.setHint(getString(R.string.address));
            }
        } else {
            editAddress.setHint(getString(R.string.address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sLANDMARK).equals("")) {
                editLandmark.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sLANDMARK));
            } else {
                editLandmark.setHint(getString(R.string.landmark));
            }
        } else {
            editLandmark.setHint(getString(R.string.landmark));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sPINCODE).equals("")) {
                editPincode.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sPINCODE));
            } else {
                editPincode.setHint(getString(R.string.pincode));
            }
        } else {
            editPincode.setHint(getString(R.string.pincode));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sSELECT_COUNTRY) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sSELECT_COUNTRY).equals("")) {
                actCountry.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sSELECT_COUNTRY));
            } else {
                actCountry.setHint(getString(R.string.select_country));
            }
        } else {
            actCountry.setHint(getString(R.string.select_country));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sSELECT_STATE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sSELECT_STATE).equals("")) {
                actState.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sSELECT_STATE));
            } else {
                actState.setHint(getString(R.string.select_state));
            }
        } else {
            actState.setHint(getString(R.string.select_state));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY)
                    .equals("")) {
                editCity.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sCITY));
            } else {
                editCity.setHint(getString(R.string.city));
            }
        } else {
            editCity.setHint(getString(R.string.city));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sPHONE_NUMBER) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sPHONE_NUMBER).equals("")) {
                editPhoneNo.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sPHONE_NUMBER));
            } else {
                editPhoneNo.setHint(getString(R.string.phone_number));
            }
        } else {
            editPhoneNo.setHint(getString(R.string.phone_number));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sIS_SHIPPING) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sIS_SHIPPING).equals("")) {
                txtIsShipping.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sIS_SHIPPING));
            } else {
                txtIsShipping.setHint(getString(R.string.is_shipping));
            }
        } else {
            txtIsShipping.setHint(getString(R.string.is_shipping));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sADD_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sADD_ADDRESS).equals("")) {
                btnAddAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sADD_ADDRESS));
            } else {
                btnAddAddress.setText(getString(R.string.add_address));
            }
        } else {
            btnAddAddress.setText(getString(R.string.add_address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sMY_ADDRESS).equals("")) {
                txtAddressName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sMY_ADDRESS));
            } else {
                txtAddressName.setText(getString(R.string.myaddress));
            }
        } else {
            txtAddressName.setText(getString(R.string.myaddress));
        }
    }//endregion


    @Override
    public void onBackPressed() {
        /*super.onBackPressed();
        finish();*/
        if (Type.equalsIgnoreCase("MainActivity")) {
            super.onBackPressed();
            finish();
        } else {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(context, MyAccountAddressListActivity.class);
            bundle.putString("ActivityType", ActivityType);
            bundle.putString("AddressType", AddressType);
            bundle.putString("AddressTypeName", "");
            intent.putExtra("BundleAddress", bundle);
            startActivity(intent);
            finish();
        }
    }

    //region AppSetting..
    @SuppressLint("NewApi")
    private void AppSetting() {
        editFirstName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editLastName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editLandmark.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editPincode.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editCity.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        txtAddShippingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtAddBillingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtAddressName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        checkBoxisShipping.setButtonTintList(ColorStateList.valueOf(Color.parseColor(
                SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                        StaticUtility.ThemePrimaryColor))));
        txtIsShipping.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
    }
    //endregion

    //region OnClickListener...
    private void OnClickListener() {
        btnAddAddress.setOnClickListener(this);
        checkBoxBilling.setOnClickListener(this);
        checkBoxisShipping.setOnClickListener(this);
        actCountry.setOnClickListener(this);
        actState.setOnClickListener(this);
        imageCartBack.setOnClickListener(this);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        editFirstName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLastName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLandmark.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPincode.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editCity.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddBillingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnAddAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddressName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtIsShipping.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region Initialization...
    private void Initialization() {
        btnAddAddress = findViewById(R.id.btnAddAddress);
        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editAddress = findViewById(R.id.editAddress);
        editLandmark = findViewById(R.id.editLandmark);
        editPincode = findViewById(R.id.editPincode);
        editCity = findViewById(R.id.editCity);
        editPhoneNo = findViewById(R.id.editPhoneNo);
        relativeProgress = findViewById(R.id.relativeProgress);

        llAddBillingAddress = findViewById(R.id.llAddBillingAddress);
        checkBoxBilling = findViewById(R.id.checkBoxBilling);
        checkBoxisShipping = findViewById(R.id.checkBoxisShipping);

        coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);

        txtAddBillingAddress = findViewById(R.id.txtAddBillingAddress);
        txtAddShippingAddress = findViewById(R.id.txtAddShippingAddress);
        txtAddressName = findViewById(R.id.txtAddressName);

        llToolbar = findViewById(R.id.llToolbar);
        imageCartBack = findViewById(R.id.imageCartBack);

        actCountry = findViewById(R.id.actCountry);
        actState = findViewById(R.id.actState);
        flCountry = findViewById(R.id.flCountry);
        flState = findViewById(R.id.flState);
        txtIsShipping = findViewById(R.id.txtIsShipping);
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddAddress:
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        btnAddAddress.getWindowToken(), 0);
                if (ischeck) {
                    if (ValidationShipping()) {
                        AddressType = "0";
                        AddShippingAddress(AddressType);
                    }
                } else {
                    if (ValidationShipping()) {
                        AddressType = "1";
                        AddShippingAddress(AddressType);
                    }
                }
                break;
            case R.id.checkBoxBilling:
                if (checkBoxBilling.isChecked()) {
                    llAddBillingAddress.setVisibility(View.GONE);
                    ischeck = false;
                } else {
                    llAddBillingAddress.setVisibility(View.VISIBLE);
                    ischeck = true;
                }
                break;

            case R.id.checkBoxisShipping:
                if (((CheckBox) view).isChecked()) {
                    ischeck = true;
                } else {
                    ischeck = false;
                }
                break;

            case R.id.actCountry:
                actCountry.showDropDown();
                break;

            case R.id.actState:
                actState.showDropDown();
                break;

            case R.id.imageCartBack:
                if (Type.equalsIgnoreCase("MainActivity")) {
                    super.onBackPressed();
                    finish();
                } else {
                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(context, MyAccountAddressListActivity.class);
                    bundle.putString("ActivityType", ActivityType);
                    bundle.putString("AddressType", AddressType);
                    bundle.putString("AddressTypeName", "");
                    intent.putExtra("BundleAddress", bundle);
                    startActivity(intent);
                    finish();
                }
                break;
        }
    }

    //region FOR GET Country API...
    private void getCountryAPI() {
        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.Countries);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        countries.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 0; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String CountryCode = jsonObjectProduct.getString("country_id");
                                        String Countryname = jsonObjectProduct.getString("country_name");

                                        country = new Country();
                                        country.setCountryName(Countryname);
                                        country.setCountryId(CountryCode);
                                        countries.add(country);
                                    }
                                    if (countries.size() > 0) {
                                        AutoCompleteForCountryAdapter autoCompleteForCountryAdapter = new
                                                AutoCompleteForCountryAdapter(context, R.layout.row_items, countries,
                                                actCountry);
                                        actCountry.setAdapter(autoCompleteForCountryAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[",
                                        "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                    "Getting error in ADDAddressesFragment.java When parsing Error response." +
                                            "\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER For COUNTRY...
    public class AutoCompleteForCountryAdapter extends ArrayAdapter<Country> {
        Context mContext;
        ArrayList<Country> mDepartments;
        ArrayList<Country> mDepartments_All;
        ArrayList<Country> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForCountryAdapter(Context context, int resource, ArrayList<Country> departments,
                                             AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public Country getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final Country department = getItem(position);
                TextView name = convertView.findViewById(R.id.ItemName);
                TextView id = convertView.findViewById(R.id.ItemId);
                name.setText(department.getCountryName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                        Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Country pi = getItem(position);
                        strCountry = pi.getCountryName();
                        if (!strCountry.equals("")) {
                            getStateAPI();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((Country) resultValue).getCountryName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (Country department : mDepartments_All) {
                            if (department.getCountryName().toLowerCase().startsWith(constraint.toString()
                                    .toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof Country) {
                                mDepartments.add((Country) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR GET State API...
    private void getStateAPI() {
        String[] key = {"country_name"};
        String[] val = {actCountry.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.States);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        states.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 1; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String country_id = jsonObjectProduct.getString("country_id");
                                        String StateId = jsonObjectProduct.getString("state_id");
                                        String Statename = jsonObjectProduct.getString("state_name");
                                        String status = jsonObjectProduct.getString("status");

                                        state = new State();
                                        state.setStateName(Statename);
                                        state.setStateId(StateId);
                                        state.setCountry_id(country_id);
                                        state.setStatus(status);
                                        states.add(state);
                                    }
                                    if (states.size() > 0) {
                                        AutoCompleteForStateAdapter autoCompleteForStateAdapter = new
                                                AutoCompleteForStateAdapter(context, R.layout.row_items, states,
                                                actState);
                                        actState.setAdapter(autoCompleteForStateAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[",
                                        "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                    "Getting error in ADDAddressesFragment.java When parsing Error response." +
                                            "\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER FOR STATE...
    public class AutoCompleteForStateAdapter extends ArrayAdapter<State> {
        Context mContext;
        ArrayList<State> mDepartments;
        ArrayList<State> mDepartments_All;
        ArrayList<State> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForStateAdapter(Context context, int resource, ArrayList<State> departments,
                                           AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public State getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final State department = getItem(position);
                TextView name = convertView.findViewById(R.id.ItemName);
                TextView id = convertView.findViewById(R.id.ItemId);
                name.setText(department.getStateName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                        Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        State pi = getItem(position);
                        strState = pi.getStateName();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((State) resultValue).getStateName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (State department : mDepartments_All) {
                            if (department.getStateName().toLowerCase().startsWith(constraint.toString()
                                    .toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof State) {
                                mDepartments.add((State) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR AddShippingAddress...
    private void AddShippingAddress(final String type) {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {"useraddress_id", "first_name", "last_name", "address", "landmark", "pincode", "country",
                "state", "city", "phone_number", "is_shipping"};
        String[] val = {"", editFirstName.getText().toString(), editLastName.getText().toString(),
                editAddress.getText().toString(),
                editLandmark.getText().toString(), editPincode.getText().toString(), actCountry.getText().toString(),
                actState.getText().toString(), editCity.getText().toString(),
                editPhoneNo.getText().toString(), type};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ADDAddresses);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    editFirstName.setText("");
                                    editLastName.setText("");
                                    editAddress.setText("");
                                    editLandmark.setText("");
                                    editPincode.setText("");
                                    editCity.setText("");
                                    editPhoneNo.setText("");
                                    getCountryAPI();
                                    /*onBackPressed();
                                    finish();*/
                                    Bundle bundle = new Bundle();
                                    Intent intent = new Intent(context, MyAccountAddressListActivity.class);
                                    bundle.putString("ActivityType", ActivityType);
                                    bundle.putString("AddressType", type);
                                    bundle.putString("AddressTypeName", "");
                                    intent.putExtra("BundleAddress", bundle);
                                    startActivity(intent);
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[",
                                        "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    if (SharedPreference.GetPreference(context,
                                            StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED)
                                            != null) {
                                        if (!SharedPreference.GetPreference(context,
                                                StaticUtility.MULTILANGUAGEPREFERENCE,
                                                StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context,
                                                    StaticUtility.MULTILANGUAGEPREFERENCE,
                                                    StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                    "Getting error in ADDAddressesFragment.java When parsing Error " +
                                            "response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region ValidationShipping
    public Boolean ValidationShipping() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editFirstName.getText().toString())) {
            editFirstName.setError(null);
            if (!TextUtils.isEmpty(editLastName.getText().toString())) {
                editLastName.setError(null);
                if (!TextUtils.isEmpty(editAddress.getText().toString())) {
                    editAddress.setError(null);
                    if (!TextUtils.isEmpty(editLandmark.getText().toString())) {
                        editLandmark.setError(null);
                        if (!TextUtils.isEmpty(editPincode.getText().toString())) {
                            editPincode.setError(null);
                            if (!TextUtils.isEmpty(editCity.getText().toString())) {
                                editCity.setError(null);
                                if (!TextUtils.isEmpty(editPhoneNo.getText().toString())) {
                                    editPhoneNo.setError(null);
                                    if (editPhoneNo.length() >= 10) {
                                        editPhoneNo.setError(null);
                                        valid = true;
                                        /*if (!txtShippingCountry.getText().equals("Please Select")) {
                                            if (!txtShippingState.getText().equals("Please Select")) {

                                            } else {
                                                Toast.makeText(context, "Please Select State", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(context, "Please Select Country", Toast.LENGTH_SHORT).show();
                                        }*/
                                    } else {
                                        if (SharedPreference.GetPreference(context,
                                                StaticUtility.MULTILANGUAGEPREFERENCE,
                                                StaticUtility.sPHONE_CHECK_VALID) != null) {
                                            if (!SharedPreference.GetPreference(context,
                                                    StaticUtility.MULTILANGUAGEPREFERENCE,
                                                    StaticUtility.sPHONE_CHECK_VALID).equals("")) {
                                                editPhoneNo.setError(SharedPreference.GetPreference(context,
                                                        StaticUtility.MULTILANGUAGEPREFERENCE,
                                                        StaticUtility.sPHONE_CHECK_VALID));
                                            } else {
                                                editPhoneNo.setError(getString(R.string.valid_phone_number_error_message));
                                            }
                                        } else {
                                            editPhoneNo.setError(getString(R.string.valid_phone_number_error_message));
                                        }
                                    }
                                } else {
                                    if (SharedPreference.GetPreference(context,
                                            StaticUtility.MULTILANGUAGEPREFERENCE,
                                            StaticUtility.sPHONE_CHECK_EMPTY) != null) {
                                        if (!SharedPreference.GetPreference(context,
                                                StaticUtility.MULTILANGUAGEPREFERENCE,
                                                StaticUtility.sPHONE_CHECK_EMPTY).equals("")) {
                                            editPhoneNo.setError(SharedPreference.GetPreference(context,
                                                    StaticUtility.MULTILANGUAGEPREFERENCE,
                                                    StaticUtility.sPHONE_CHECK_EMPTY));
                                        } else {
                                            editPhoneNo.setError(getString(R.string.null_phone_number_error_message));
                                        }
                                    } else {
                                        editPhoneNo.setError(getString(R.string.null_phone_number_error_message));
                                    }
                                }
                            } else {
                                if (SharedPreference.GetPreference(context,
                                        StaticUtility.MULTILANGUAGEPREFERENCE,
                                        StaticUtility.sCITY_CHECK_EMPTY) != null) {
                                    if (!SharedPreference.GetPreference(context,
                                            StaticUtility.MULTILANGUAGEPREFERENCE,
                                            StaticUtility.sCITY_CHECK_EMPTY).equals("")) {
                                        editCity.setError(SharedPreference.GetPreference(context,
                                                StaticUtility.MULTILANGUAGEPREFERENCE,
                                                StaticUtility.sCITY_CHECK_EMPTY));
                                    } else {
                                        editCity.setError(getString(R.string.null_city_error_message));
                                    }
                                } else {
                                    editCity.setError(getString(R.string.null_city_error_message));
                                }
                            }
                        } else {
                            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                    StaticUtility.sPINCODE_CHECK_EMPTY) != null) {
                                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                        StaticUtility.sPINCODE_CHECK_EMPTY).equals("")) {
                                    editPincode.setError(SharedPreference.GetPreference(context,
                                            StaticUtility.MULTILANGUAGEPREFERENCE,
                                            StaticUtility.sPINCODE_CHECK_EMPTY));
                                } else {
                                    editPincode.setError(getString(R.string.null_pincode_error_message));
                                }
                            } else {
                                editPincode.setError(getString(R.string.null_pincode_error_message));
                            }
                        }
                    } else {
                        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                StaticUtility.sLANDMARK_CHECK_EMPTY) != null) {
                            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                    StaticUtility.sLANDMARK_CHECK_EMPTY).equals("")) {
                                editLandmark.setError(SharedPreference.GetPreference(context,
                                        StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY));
                            } else {
                                editLandmark.setError(getString(R.string.null_landmark_error_message));
                            }
                        } else {
                            editLandmark.setError(getString(R.string.null_landmark_error_message));
                        }
                    }
                } else {
                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                            StaticUtility.sADDRESS_CHECK_EMPTY) != null) {
                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                StaticUtility.sADDRESS_CHECK_EMPTY).equals("")) {
                            editAddress.setError(SharedPreference.GetPreference(context,
                                    StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY));
                        } else {
                            editAddress.setError(getString(R.string.null_address_error_message));
                        }
                    } else {
                        editAddress.setError(getString(R.string.null_address_error_message));
                    }
                }
            } else {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sLASTNAME_CHECK_EMPTY) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                            StaticUtility.sLASTNAME_CHECK_EMPTY).equals("")) {
                        editLastName.setError(SharedPreference.GetPreference(context,
                                StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY));
                    } else {
                        editLastName.setError(getString(R.string.null_lname_error_message));
                    }
                } else {
                    editLastName.setError(getString(R.string.null_lname_error_message));
                }
            }
        } else {
            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sFIRSTNAME_CHECK_EMPTY) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sFIRSTNAME_CHECK_EMPTY).equals("")) {
                    editFirstName.setError(SharedPreference.GetPreference(context,
                            StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY));
                } else {
                    editFirstName.setError(getString(R.string.null_fname_error_message));
                }
            } else {
                editFirstName.setError(getString(R.string.null_fname_error_message));
            }
        }
        return valid;
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)
                != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context,
                    Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ButtonTextColor)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ThemePrimaryColor)));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    // region chanage FrameLayout Border
    public void chanageFramelayoutBorder(FrameLayout frameLayout, AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        frameLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) frameLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setColor(Color.parseColor("#FFFFFF"));
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)
                != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context,
                    Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region Keyboard Hide...
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS),
                                    0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
