package com.pureweblopment.theeleven.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.theeleven.Adapter.CommissionAdapter;
import com.pureweblopment.theeleven.Global.Global;
import com.pureweblopment.theeleven.Global.SendMail;
import com.pureweblopment.theeleven.Global.SharedPreference;
import com.pureweblopment.theeleven.Global.StaticUtility;
import com.pureweblopment.theeleven.Global.Typefaces;
import com.pureweblopment.theeleven.Model.Commission;
import com.pureweblopment.theeleven.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class CommissionScreen extends AppCompatActivity implements View.OnClickListener {
    private Context mContext = CommissionScreen.this;

    //Toolbar
    private FrameLayout mFlToolbar;
    private ImageView mImageCartBack;
    private TextView mTxtCatName;
    //widget
    private RecyclerView mRvCommission;
    private TextView mTxtNoCommission;
    //Loader
    private ProgressBar mProgress;
    private RelativeLayout mRlProgress;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    //Pagination
    ArrayList<Commission> commissions;
    private boolean mCommissionLoading = true;
    private boolean mCommissionSetup = true;
    private int mCommissionLimit = 10, mCommissionOffset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_commission_screen);
        eventBus.register(this);

        Initialization();
        TypeFace();
        OnClickListener();
        AppSetting();
        setDynamicLabel();

        if (Global.isNetworkAvailable(mContext)) {
            getCommissionAPI();
        } else {
            openInternetAlertDialog(mContext, "Not connected to Internet");
        }

        mRvCommission.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                //position starts at 0
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                int itemCount = layoutManager.getItemCount();
                if (lastVisibleItemPosition >= layoutManager.getItemCount() - 5) {
                    if (mCommissionLoading) {
                        mCommissionLoading = false;
                        mCommissionSetup = false;
                        getCommissionAPI();
                    }
                }
            }
        });
    }

    private void setDynamicLabel() {
        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION).equals("")) {
                mTxtCatName.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION));
            } else {
                mTxtCatName.setText(getString(R.string.commission));
            }
        } else {
            mTxtCatName.setText(getString(R.string.commission));
        }
        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_MORE_COMMISSION_CHECK_EMPTY) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_MORE_COMMISSION_CHECK_EMPTY).equals("")) {
                mTxtNoCommission.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_MORE_COMMISSION_CHECK_EMPTY));
            } else {
                mTxtNoCommission.setText(getString(R.string.no_more_commission));
            }
        } else {
            mTxtNoCommission.setText(getString(R.string.no_more_commission));
        }

        /*if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION) != null) {
            mTxtNoCommission.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION));
        } else {
            mTxtNoCommission.setText(getString(R.string.no_more_commission));
        }*/
    }

    //region Initialization
    private void Initialization() {
        //Loader
        mProgress = findViewById(R.id.progress);
        mRlProgress = findViewById(R.id.rlProgress);
        //Toolbar
        mFlToolbar = findViewById(R.id.flToolbar);
        mImageCartBack = findViewById(R.id.imageCartBack);
        mTxtCatName = findViewById(R.id.txtCatName);
        //Widget
        mRvCommission = findViewById(R.id.rvCommission);
        mTxtNoCommission = findViewById(R.id.txtNoCommission);
    }//endregion

    //region TypeFace
    private void TypeFace() {
        mTxtCatName.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        mTxtNoCommission.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
    }//endregion

    //region OnClickListener
    private void OnClickListener() {
        mImageCartBack.setOnClickListener(this);
    }//endregion

    //region AppSetting
    private void AppSetting() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mProgress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }
        mFlToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
    }//endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(mContext, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(mContext)) {
                openInternetAlertDialog(mContext, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

    //region FOR GET Commission API...
    private void getCommissionAPI() {
        if (mCommissionSetup) {
            mRlProgress.setVisibility(View.VISIBLE);
        }
        String[] key = {"offset", "limit"};
        String[] val = {String.valueOf(mCommissionOffset), String.valueOf(mCommissionLimit)};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCommission);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(mContext));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mRlProgress.setVisibility(View.GONE);
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONObject jsonObjectpayload = response.optJSONObject("payload");
                                    JSONArray jsonArrayCommission = jsonObjectpayload.getJSONArray("commission");
                                    if (jsonArrayCommission.length() > 0) {
                                        mTxtNoCommission.setVisibility(View.GONE);
                                        mRvCommission.setVisibility(View.VISIBLE);
                                        if (mCommissionSetup) {
                                            commissions = new ArrayList<>();
                                        }
                                        for (int i = 0; i < jsonArrayCommission.length(); i++) {
                                            JSONObject jsonObjectCommission = jsonArrayCommission.getJSONObject(i);
                                            commissions.add(new Commission(jsonObjectCommission.optString("commission_id"),
                                                    jsonObjectCommission.optString("commission_percentage"),
                                                    jsonObjectCommission.optString("commission_amount"),
                                                    jsonObjectCommission.optString("z_orderid_fk"),
                                                    jsonObjectCommission.optString("z_userid_fk"),
                                                    jsonObjectCommission.optString("commission_ok_reason"),
                                                    jsonObjectCommission.optString("commission_reject_reason"),
                                                    jsonObjectCommission.optString("user_name"),
                                                    jsonObjectCommission.optString("order_id")));

                                        }

                                        if (mCommissionSetup) {
                                            mCommissionLoading = true;
                                            mCommissionOffset = mCommissionOffset + mCommissionLimit;
                                            CommissionAdapter commissionAdapter = new CommissionAdapter(mContext, commissions);
                                            mRvCommission.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                                            mRvCommission.setAdapter(commissionAdapter);
                                        } else {
                                            if (jsonArrayCommission.length() == mCommissionLimit) {
                                                mCommissionOffset = mCommissionOffset + mCommissionLimit;
                                                mCommissionLoading = true;
                                            } else {
                                                mCommissionLoading = false;
                                            }
                                            mRvCommission.getAdapter().notifyDataSetChanged();
                                        }
                                    } else {
                                        if (mCommissionSetup) {
                                            mTxtNoCommission.setVisibility(View.VISIBLE);
                                            mRvCommission.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                mRlProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("400")) {
                                    MainActivity.manageBackPress(true);
                                    if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(mContext, SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(mContext, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(mContext, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(mContext, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(mContext, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(mContext, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(mContext, Global.Billing_Preference);
                                    Intent intent = new Intent(mContext, LoginActivity.class);
                                    startActivity(intent);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageCartBack:
                finish();
                break;
        }
    }
}
