package com.pureweblopment.theeleven.Model;

public class PaymentGateway {
    String name;
    String slug;
    String pgconst;
    boolean isSelect;

    public PaymentGateway(String name, String slug, String pgconst, boolean isSelect) {
        this.name = name;
        this.slug = slug;
        this.pgconst = pgconst;
        this.isSelect = isSelect;
    }

    public String getPgconst() {
        return pgconst;
    }

    public String getSlug() {
        return slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
