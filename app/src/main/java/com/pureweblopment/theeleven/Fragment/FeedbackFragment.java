package com.pureweblopment.theeleven.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.theeleven.Activity.LoginActivity;
import com.pureweblopment.theeleven.Activity.MainActivity;
import com.pureweblopment.theeleven.Global.Global;
import com.pureweblopment.theeleven.Global.SendMail;
import com.pureweblopment.theeleven.Global.SharedPreference;
import com.pureweblopment.theeleven.Global.StaticUtility;
import com.pureweblopment.theeleven.Global.Typefaces;
import com.pureweblopment.theeleven.Model.Feedback;
import com.pureweblopment.theeleven.Model.Item;
import com.pureweblopment.theeleven.R;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FeedbackFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeedbackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedbackFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation;
    CardView cardviewBottomNavigation;

    RecyclerView recyclerviewOrderFeedback;
    RelativeLayout relativeProgress;
    LinearLayout llNoOrderAvailable;
    TextView txtNoOrderAvailable;

    String image, name;

    private List<Item> postdata;
    private ArrayList<HashMap<String, String>> postList = new ArrayList<HashMap<String, String>>();
    private boolean hasMore;
    int limit = 10, offset = 0;

    AdapterOrderHistory adapterOrderHistory;

    private OnFragmentInteractionListener mListener;

    public FeedbackFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FeedbackFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FeedbackFragment newInstance(String param1, String param2) {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        imageCartBack = getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = getActivity().findViewById(R.id.imageNavigation);
        imageLogo = getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);

        Initialization(view);
        TypeFace();
        AppSettings();

        if (postdata != null) {
            try {
                adapterOrderHistory.clearData();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            postdata.clear();
            postList.clear();
        }

        recyclerviewOrderFeedback.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (hasMore) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int itemCount = layoutManager.getItemCount();
                    if (layoutManager.findLastVisibleItemPosition() >= layoutManager.getItemCount() - 5) {
                        FeedbackListing(false);
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            FeedbackListing(true);
        } else {
            llNoOrderAvailable.setVisibility(View.VISIBLE);
        }
    }

    //region Initialization
    private void Initialization(View view) {
        recyclerviewOrderFeedback = view.findViewById(R.id.recyclerviewOrderFeedback);
        relativeProgress = view.findViewById(R.id.relativeProgress);
        llNoOrderAvailable = view.findViewById(R.id.llNoOrderAvailable);
        txtNoOrderAvailable = view.findViewById(R.id.txtNoOrderAvailable);
    }//endregion

    //region TypeFace
    private void TypeFace() {
        txtNoOrderAvailable.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }//endregion

    //region AppSettings
    private void AppSettings() {
        txtNoOrderAvailable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK) != null) {
        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK).equals("")) {
            txtCatName.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK));
        }else {
            txtCatName.setText(R.string.feedback);
        }
        } else {
            txtCatName.setText(R.string.feedback);
        }
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoSubmitFeedback(String product_id, String orderitem_id);
    }

    //region FOR FeedbackListing...
    private void FeedbackListing(final boolean isSetUp) {
        if (isSetUp) {
            relativeProgress.setVisibility(View.VISIBLE);
        }
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        String[] key = {"offset", "limit", "currencyCode"};
        String[] val = {String.valueOf(offset), String.valueOf(limit), strCurrencyCode};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Feedback);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONArray jsonArray = response.getJSONArray("payload");
                                    ArrayList<Item> orderItems = new ArrayList<Item>(10);
//                                    feedbacks = new ArrayList<>();
                                    if (jsonArray.length() > 0) {
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            JSONObject jsonObjectImage = jsonObject.getJSONObject("main_image");
                                            String image = jsonObjectImage.getString("main_image");
                                            String feedback_id = jsonObject.getString("feedback_id");
                                            String order_id = jsonObject.getString("order_id");
                                            String price = jsonObject.getString("price");
                                            String datetime = jsonObject.getString("delievery_datetime");
                                            String name = jsonObject.getString("name");
                                            String order_item_status = jsonObject.getString("order_item_status");
                                            String product_id = jsonObject.getString("product_id");
                                            String orderitem_id = jsonObject.getString("orderitem_id");
                                            String currency_symbol = jsonObject.getString("currency_symbol");
                                            String currency_place = jsonObject.getString("currency_place");
                                            Feedback feedback = new Feedback(name, order_id, datetime, price, feedback_id, image, order_item_status, product_id, orderitem_id, currency_symbol, currency_place);
                                            orderItems.add(feedback);
                                        }

                                        if (!isSetUp) {
                                            offset = offset + limit;
                                            if (orderItems.size() > 0 && hasMore) {
                                                hasMore = true;
                                                postdata.addAll(orderItems);
                                                recyclerviewOrderFeedback.setHasFixedSize(true);
                                                recyclerviewOrderFeedback.getAdapter().notifyDataSetChanged();
                                            } else {
                                                hasMore = false;
                                                recyclerviewOrderFeedback.getAdapter().notifyDataSetChanged();
                                            }
                                            if (limit > orderItems.size()) {
                                                hasMore = false;
                                            }
                                        } else {
                                            postdata = orderItems;
                                            offset = offset + limit;
                                            if (postdata.size() > 0) {
                                                hasMore = true;
                                                llNoOrderAvailable.setVisibility(View.GONE);
                                                recyclerviewOrderFeedback.setVisibility(View.VISIBLE);
                                                AdapterOrderHistory adapterOrderHistory = new AdapterOrderHistory(getContext(), postdata);
                                                recyclerviewOrderFeedback.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                                recyclerviewOrderFeedback.setAdapter(adapterOrderHistory);
                                            } else {
                                                hasMore = false;
                                                llNoOrderAvailable.setVisibility(View.VISIBLE);
                                                recyclerviewOrderFeedback.setVisibility(View.GONE);
                                            }
                                        }
                                    } else {
                                        if (!isSetUp) {
                                            hasMore = false;
                                            recyclerviewOrderFeedback.getAdapter().notifyDataSetChanged();
                                        } else {
                                            hasMore = false;
                                            llNoOrderAvailable.setVisibility(View.VISIBLE);
                                            recyclerviewOrderFeedback.setVisibility(View.GONE);
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in FeedbackFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterOrderHistory...
    public class AdapterOrderHistory extends RecyclerView.Adapter<AdapterOrderHistory.Viewholder> {

        Context context;
        JSONArray jsonArray;
        List<Item> orders;

        public AdapterOrderHistory(Context context, List<Item> orders) {
            this.orders = orders;
            this.context = context;
        }

        @Override
        public AdapterOrderHistory.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_feedback, viewGroup, false);
            return new AdapterOrderHistory.Viewholder(view);
        }

        public void clearData() {
            int size = orders.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    orders.remove(0);
                }
                this.notifyItemRangeRemoved(0, size);
            }
        }

        @Override
        public void onBindViewHolder(final Viewholder holder, int position) {
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                }
            }
//            final Feedback feedback = feedbacks.get(position);
            final Feedback feedback = (Feedback) orders.get(position);
            String feedback_id = feedback.getFeedback_id();

            holder.txtProductName.setText(feedback.getOrdername());
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID).equals("")) {
                holder.txtOrderID.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID) + " " + feedback.getOrderId());
            }else {
                holder.txtOrderID.setText(getString(R.string.order_id) + " " + feedback.getOrderId());
            }
            } else {
                holder.txtOrderID.setText(getString(R.string.order_id) + " " + feedback.getOrderId());
            }
            holder.txtProducDate.setText(Global.changeDateFormate(feedback.getOrderDate(),
                    "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
            float floatPrice = Float.parseFloat(feedback.getPrice());
            int intPrice = Math.round(floatPrice);

            if (feedback.getCurrency_position().equals("1")) {
                holder.txtProductPrice.setText(feedback.getCurrency_symbol() + " " + String.valueOf(intPrice));
            } else if (feedback.getCurrency_position().equals("0")) {
                holder.txtProductPrice.setText(String.valueOf(intPrice) + " " + feedback.getCurrency_symbol());
            }

            String image = feedback.getImage();
            String image1 = image.replace("[", "").replace("]", "").replace("\"", "");
            holder.txtProductStatus.setText(getString(R.string.delivered));
            holder.txtProductStatus.setBackgroundColor(Color.parseColor("#008000"));
            holder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(image1.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(holder.imageProduct, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                                holder.rlImgHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in FeedbackFragment to ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in FeedbackFragment to ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

            holder.txtFeedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    offset = 0;
                    String product_id = feedback.getProduct_id();
                    String orderitem_id = feedback.getOrderitem_id();
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoSubmitFeedback(product_id, orderitem_id);
                }
            });

            if (feedback_id.equals("")) {
                holder.txtFeedback.setVisibility(View.VISIBLE);
            } else {
                holder.txtFeedback.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
//            return feedbacks.size();
            return orders.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct;
            TextView txtProductName, txtOrderID, txtProductPrice, txtProductStatus, txtProducDate, txtFeedback;
            LinearLayout llOrderhistory, llfeedbackmain;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                txtProductName = itemView.findViewById(R.id.txtProductName);
                txtOrderID = itemView.findViewById(R.id.txtOrderID);
                txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
                txtProductStatus = itemView.findViewById(R.id.txtProductStatus);
                txtProducDate = itemView.findViewById(R.id.txtProducDate);
                txtFeedback = itemView.findViewById(R.id.txtFeedback);
                imageProduct = itemView.findViewById(R.id.imageProduct);
                llOrderhistory = itemView.findViewById(R.id.llOrderhistory);
                llfeedbackmain = itemView.findViewById(R.id.llfeedbackmain);
                rlImgHolder = itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProductPrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txtFeedback.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProducDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                    txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                    txtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                    txtProducDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                }
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                    txtProductPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                }
                txtFeedback.setBackgroundColor(Color.parseColor("#ffffff"));
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK).equals("")) {
                    txtFeedback.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK));
                }else {
                    txtFeedback.setText(context.getString(R.string.feedback));
                }
                }else {
                    txtFeedback.setText(context.getString(R.string.feedback));
                }

            }
        }

    }
    //endregion
}
